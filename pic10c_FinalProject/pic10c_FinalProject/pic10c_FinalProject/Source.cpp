//Hadden Wright
//PIC 10 C
//Winter 2018 
//Final project
//Basic Statistical Analysis Program
//This program will run asl for numerical means or population proportions and standard deviations and compute a confidence interval for the population parameter.

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <cmath>
#include <algorithm>
#include <numeric>

int main() {
    
	std::string repeat;
	std::string testType;
    std::string dataStyle;
	std::cout << "Welcome to the BSAP" << std::endl;
    
	do {//outermost loop to run while repeat so that the user can do multiple tests
        //gets the test type. loops if entered value is incorrect
    
		std::cout << "\nWhat type of analysis will we be doing? (Please type 'ht' for Hypothesis Test or  'ci' for Confidence Interval) \n";
		do {
           
            std::cin >> testType;
            if (testType != "ci" && testType != "ht"){
				std::cout << "\nInvalid analysis type. Please type 'ht' for Hypothesis Test or  'ci' for Confidence Interval. \n";
			}
		} while (testType != "ci" && testType != "ht");
     
        //gets the data style for which we will perform an analysis.
        //descriptive statistics are the easy case because the only processes needed to be done are simple calculations
        //If it is a raw data set, we will need an ifstream and generic algorithms to compute descriptive statistics
        double stdDev;
        double mean;
        double sampleSize;
        std::string datatype;
        
        //get the dataType
        std::cout<< "\nIs this a numerical data or are we looking for a proportion? Please type 'n' for numerical or 'p' for proportion.\n";
        do{
            std::cin >> datatype;
            if (datatype != "n" && datatype != "p"){
                std::cout << "\nInvalid data style. Please type 'm' for mean or 'p' for proportion. \n";
            }
        }while(datatype != "n" && datatype != "p");
        
        std::cout<< "\nDo you have descriptive statistics or just a raw dataset? (Please type 'ds' for descriptive statistics or 'raw' for a raw dataset).\n";
        do {
            std::cin >> dataStyle;
            if (dataStyle != "ds" && dataStyle != "raw"){
                std::cout << "\nInvalid data style. Please type 'ds' for descriptive statistics or 'raw' for a raw dataset. \n";
            }
        } while (dataStyle != "ds" && dataStyle != "raw");
        
        
        //descriptive statistics are to be given
        if (dataStyle == "ds"){
            std::cout<< "\nWhat was the sample size? \n";
            std::cin>>sampleSize;
            
            if (datatype == "n"){
                std::cout<< "\nWhat was the mean?\n";
                std::cin >>mean;
                std::cout << "\nWhat was the standard deviation?\n";
                std::cin >>stdDev;
                
            }
            if (datatype == "p"){
                std::cout<< "\nWhat was the sample proportion?\n";
                do{
                std::cin >>mean;
                    if(mean<0 || mean >1){
                        std::cout<< "\nInvalid proportion, try again.\n";
                    }
                }while(mean<0 || mean >1);
            }
            
        }
        //must import data
        if (dataStyle == "raw"){
            std::string filename;
            std::vector<double> data;
            
            
            std::cout<< "\nWhat is the file name?\n";
            std::cin >> filename;
            
    
            std::ifstream inputData(filename);
            char delim;
            std::string temp;
            std::cout<< "\nWhat is the delimiting character?\n";
            std::cin>>delim;
            
            while(getline(inputData, temp, delim)){ // while getline works, ie until we reach the end of the file. This assumes the file is formatted properly, if data is incorrectly input then this will stop whenever that happens
                data.push_back(std::stod(temp));
            }
                 
                
            sampleSize = data.size();
    
            
            //compute mean and standard deviation
            if (datatype == "p"){
                 mean = (std::accumulate(data.begin(), data.end(), 0 ) / sampleSize);
                if(mean > 1 ){
                    std::cout << "\nThis is data does not correspond to a proportion, will analyze as a numerical.\n";
                    datatype = "n";
                }
            }
            if (datatype == "n"){
                mean = 0;

                //calculate arithmetic mean.
                std::for_each(data.begin(),
                              data.end(),
                              [&mean, sampleSize] (double x) {mean += x/sampleSize;}
                              );

                double sumOfSquareDifferences = 0;
                
                // find the deviation, square it, then add to sum of squares for each element in the data set
                std::for_each(std::begin(data),
                              std::end(data),
                              [mean, &sumOfSquareDifferences, sampleSize] (double& x)
                              {
                                  x -= mean ;
                                  x *= x;
                                  sumOfSquareDifferences += x/sampleSize ;
                              }
                              );
                //calculate standard deviation
                
                stdDev = sqrt( sumOfSquareDifferences / (sampleSize));
            
            }
        }
            double stdError;
        //calculate standard Error
            if (datatype == "p"){
                stdError  = sqrt((mean * (1-mean) / sampleSize));
            }
            if (datatype == "n"){
                stdError = stdDev / sqrt(sampleSize);
            }
        //output result of analysis
        if( testType == "ci"){
            std::string confidence;
            std::cout << "\nHow confident do you want the interval? Please type '90%' '95%' or '99%'\n";
            do{
                
                std::cin >> confidence;
                if(confidence != "90%" && confidence != "95%" && confidence != "99%" ){
                    std::cout << "\nInvalid confidence level. Please type '90%' '95%' or '99%'\n";
                }
            }while(confidence != "90%" && confidence != "95%" && confidence != "99%" );
            
            if(confidence == "90%"){
                double bottomBound = mean - (1.7 * stdError);
                double topBound = mean + (1.7 * stdError);
                std::cout << "\nI am 90 % confident that the population parameter lies within "<< bottomBound << " and " << topBound<< ".\n";
            }
            else if(confidence == "95%"){
                double bottomBound =mean - (1.96 * stdError);
                double topBound = mean + (1.96 * stdError);
                std::cout << "\nI am 95 % confident that the population parameter lies within "<< bottomBound << " and " << topBound<< ".\n";
                
            }
            else if(confidence == "99%"){
                double bottomBound =mean - (2.7 * stdError);
                double topBound = mean + (2.7 * stdError);
                std::cout << "\nI am 99 % confident that the population parameter lies within "<< bottomBound << " and " << topBound<< ".\n";
                
            }
            
        }
    
    if (testType == "ht"){
        std::string alpha ;
        double hyp;
        double testStat;
        std::string hypType;
        std::cout<< "\nAt what significance level do you perform this analysis? Please type '.01' '.05' or '.1'\n";
        do{
            
            std::cin >> alpha;
            if(alpha != ".01" && alpha != ".05" && alpha != ".1"){
                std::cout << "\nInvalid significance level. Please type '.01' '.05' or '.1'\n";

            }
        }while(alpha != ".01" && alpha != ".05" && alpha != ".1");
        
                std::cout<< "\nWhat is the alternative hypothesis? Please type '<' '>' or '!=' \n(Format as if 'hypothesis' comparator 'parameter' \nExample: population prop < .6 \n";
                do{
                    std::cin >> hypType;
                    if (hypType != ">" && hypType != "<" && hypType != "!="){
                        std::cout << "\nInvalid hypothesis. Please type '<' '>' or '!='\n";

                    }
                }while(hypType != ">" && hypType != "<" && hypType != "!=");
                
                    if(hypType == ">" || hypType == "<" || hypType == "!="){
                        
                        if( datatype == "p"){
                            std::cout<< "\nWhat is the population proportion of intrest? \n";
                            std::cin >> hyp;
                        }
                        if (datatype == "n"){
                            std::cout<< "\nWhat is the population mean of interest? \n";
                            std::cin >> hyp;
                        }
                        testStat = (hyp - mean )/stdError; // compute z-score
                        
                        std::cout<< "\nAlternative hypothesis:  parameter " << hypType << " " <<hyp << "\nNull hypothesis: parameter = " <<hyp ;
                        std::cout << "\nWith a sample statistic of: " <<mean;
                        if (hypType == ">" ){
                            if(alpha == ".01"){
                                if(testStat < 2.33 ){
                                    std::cout << "\nWe reject the null.\n";
                                }
                                else{
                                    std::cout<< "\nWe fail to reject the null.\n";
                                }
                            }else if(alpha == ".05"){
                                if(testStat < 1.65 ){
                                    std::cout << "\nWe reject the null.\n";
                                }
                                else{
                                    std::cout<< "\nWe fail to reject the null.\n";
                                }
                            }else if(alpha == ".10"){
                                if(testStat < 1.29 ){
                                    std::cout << "\nWe reject the null.\n";
                                }
                                else{
                                    std::cout<< "\nWe fail toreject the null.\n";
                                }
                            }
                            
                        }else if(hypType == "<"){
                            if(alpha == ".01"){
                                if(testStat > 2.33 ){
                                    std::cout << "\nWe reject the null.\n";
                                }
                                else{
                                    std::cout<< "\nWe fail to reject the null.\n";
                                }
                            }else if(alpha == ".05"){
                                if(testStat > 1.65 ){
                                    std::cout << "\nWe reject the null.\n";
                                }
                                else{
                                    std::cout<< "\nWe fail to reject the null.\n";
                                }
                            }else if(alpha == ".10"){
                                if(testStat > 1.29 ){
                                    std::cout << "\nWe reject the null.\n";
                                }
                                else{
                                    std::cout<< "\nWe fail to reject the null.\n";
                                }
                            }
                        }else if(hypType == "!="){
                            if(alpha == ".01"){
                                if(testStat < -2.58 || testStat > 2.58 ){
                                    std::cout << "\nWe reject the null.\n";
                                }
                                else{
                                    std::cout<< "\nWe fail to reject the null.\n";
                                }
                            }else if(alpha == ".05"){
                                if(testStat < -1.96 || testStat > 1.96 ){
                                    std::cout << "\nWe reject the null.\n";
                                }
                                else{
                                    std::cout<< "\nWe fail to reject the null.\n";
                                }
                            }else if(alpha == ".10"){
                                if(testStat < -1.65 || testStat > 1.65 ){
                                    std::cout << "\nWe reject the null.\n";
                                }
                                else{
                                    std::cout<< "\nWe fail to reject the null.\n";
                                }
                            }
                        }
                    }
    }
            

		std::cout << "\nWould you like do perform another analysis? Answer yes/no \n" ;
        std::cin >> repeat;
        }while (repeat == "yes");
}

