* This is a program to calculate confidence intervals and perform hypothesis tests.
* This is my Final Project for the class at ucla titled PIC 10 C.
* The intent of this assignment was to use the knowledge gained from the course and apply it to creating something of the students choosing.

* Functionalities inculded are: 
	* calculating confidence intervals for population proportion when given descriptive statistics.
	* calculating confidence intervals when given a raw dataset. 
	* hypothesis tests under both scenarios.
	
* Functionalities yet to be inculded are:
	* Qt layout
	
* Things worth noting: 
	* When reading in files, the file extention does not matter assuming it can be read as a plain text file. 
	* Program is optimized for proportions because to perform proper analysis for population mean a t-test statistic should be used; but, those are dependent on degrees of freedom (samplse size). To properly implement this, this program would have to contain tabulated values for an infinite number sample sizes. I opted to simply approximate the t-stat with a z-test statistic.

* Topics from the course that are applied in this project:
	* Generic algorithms: In order to compute the mean and standard deviation I used multiple generic algorithms, for_each and accumulate.
	* Iterators: A necessary part of generic algorithms, implicitly used them in the generic algorthims above.
	* Lambda functions: The for_each algorithms required me to write lambdas in order for my calculations to be correct, as the calculations are rather niche and the algorthims for them did not exist prior.
	* C++ STL containers: Had to asses what would be the best data structure for storing potentially a lot of data. Opted for std::vector because it guarantees O(1) access. 
	* Version Control Software: Experienced some issues not being able to push my local changes, discovered differences in Sourcetree on my MAC vs my PC.

* Topics from the course that are not applied in this project:
	* Move semantics, binders and adapters, Smart pointers, Memory management, Templates, Inheritance and polymorphism, Function pointers, Introduction to Qt 
	

